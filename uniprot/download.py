import requests
import sys
class UniprotSwissDownloader():
    def __init__(self, url = "https://ftp.ebi.ac.uk/pub/databases/uniprot/current_release/knowledgebase/complete/uniprot_sprot.dat.gz") -> None:
        self.url = url


    def download(self):
        r= requests.get(self.url, stream=True)
        total_length = r.headers.get('content-length')

        with open("uniprot_sprot.dat.gz", 'wb') as f:
            if total_length is None: # no content length header
                f.write(r.content)
            else:
                dl = 0
                total_length = int(total_length)
                for data in r.iter_content(chunk_size=4096):
                    dl += len(data)
                    f.write(data)
                    done = int(50 * dl / total_length)
                    sys.stdout.write("\rDownloading[%s%s]" % ('=' * done, ' ' * (50-done)) )    
                    sys.stdout.flush()
    