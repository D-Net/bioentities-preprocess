from pdb.pdb_download import PDBDownloader
from pdb.pdb_metadata_extractor import MetadataExctractor
from pdb.pdb_validator import validate
from uniprot.download import UniprotSwissDownloader
from uniprot.metadata import MetadataExctractor as ME
from uniprot.uniprot_validator import validate as validate_uniprot

if __name__ == '__main__':
    print("UNIPROT -PREPROCESSING")
    u = UniprotSwissDownloader()
    u.download()
    k = ME()
    k.extract_metadata()
    error, valid, total, error_record = validate_uniprot()
    print(error)
    print(f"Valid {valid}/{total}")
    print(f"Error {error_record}/{total}")
    print("PDB -PREPROCESSING")
    p = PDBDownloader()
    p.get_file_to_downloads(max_item=4)
    m = MetadataExctractor()
    m.extract_metadata()
    error, valid, total, error_record = validate()
    print(error)
    print(f"Valid {valid}/{total}")
    print(f"Error {error_record}/{total}")


    
